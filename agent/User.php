<?php
namespace agent;

class User
{
    public function get() : array {
        
    }
    
    public function getById(int $id) : array {
        
    }
    
    public function getByName(string $username_email) : array {
        
    }
    
    public function getBySearch(string $search_term) : array {
        
    }
    
    public function post(array $user_data) : array {
        
    }
    
    public function put(int $id, array $user_data) : array {
        
    }
    
    public function delete(int $id) : array {
        
    }
}

