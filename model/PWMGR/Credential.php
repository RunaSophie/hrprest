<?php
namespace model\PWMGR;

class Credential
{
    /**
     * @var int $credential_id internal ID
     */
    public $credential_id;
    
    /**
     * @var int $owner username of the owner
     */
    public $owner;
    
    /**
     * @var int $category_id category ID
     */
    public $category_id;
    
    /**
     * @var string $access_status access status [owner, full, write, read]
     */
    public $access_status;
    
    /**
     * @var string $name credential name
     */
    public $name;
    
    /**
     * @var string $domain domain to be logged into
     */
    public $domain;
    
    /**
     * @var string $username username to be used
     */
    public $username;
    
    /**
     * @var string $password password to be used
     */
    public $password;
    
    /**
     * @var string $description credential description
     */
    public $description;
}

