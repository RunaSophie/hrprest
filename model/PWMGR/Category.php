<?php
namespace model\PWMGR;

class Category
{
    /**
     * @var int $category_id internal ID
     */
    public $category_id;
    
    /**
     * @var string $name category name
     */
    public $name;
    
    /**
     * @var array $credentials array of all credentials
     */
    public $credentials;
    
    /**
     * @var string $encryption_iv synchronous encryption IV
     */
    public $encryption_iv;
}

