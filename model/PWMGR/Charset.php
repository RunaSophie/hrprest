<?php
namespace model\PWMGR;

class Charset
{
    /**
     * @var string $name character set name
     */
    public $name;
    
    /**
     * @var string $charset string of the characters of the charset
     */
    public $charset;
    
    /**
     * @var int $min_chars minimum characters of this charset included in generated password
     */
    public $min_chars;
    
    /**
     * @var int $max_chars maximum characters of this charset included in generated password
     */
    public $max_chars;
}

