<?php
namespace model\PWMGR;

class Preset
{
    /**
     * @var int $preset_id internal ID
     */
    public $preset_id;
    
    /**
     * @var int $creator username of the creator
     */
    public $creator;
    
    /**
     * @var string $name preset name
     */
    public $name;
    
    /**
     * @var int $length length of the password to be generated
     */
    public $length;
    
    /**
     * @var int $max_occurrences maximum number of the same character in a generated password
     */
    public $max_occurrences;
    
    /**
     * @var array $charsets charsets
     */
    public $charsets;
}

