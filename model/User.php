<?php
namespace model;

class User
{
    /**
     * @var int $user_id internal user ID
     */
    public $user_id;
    
    /**
     * @var string $username unique username
     */
    public $username;
    
    /**
     * @var string $email unique email address
     */
    public $email;
    
    /**
     * @var string $password_hash BCRYPT hashed password for login checks
     */
    public $password_hash;
    
    /**
     * @var string $public_key user's public key
     */
    public $public_key;
    
    /**
     * @var string $private_key user's private key (password-encrypted)
     */
    public $private_key;
    
    /**
     * @var int $default_pwgen_preset user's default PWMGR password preset
     */
    public $default_pwgen_preset;

    /**
     * @var string $status user status (active, dropped)
     */
    public $status;
}

