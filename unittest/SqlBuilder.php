<?php
namespace unittest;

$no_json = true;
require_once dirname(__DIR__) . '/helper/generalRequires.php';
require_once dirname(__DIR__) . '/helper/SqlBuilder/Builder.php';

?>
<h1>SQL Builder</h1>
<h2>Test 1</h2>
Select * from table
<?php
$table = 'testTable';
var_dump($table);
$fields = [
    '*'
];
var_dump($fields);
$sql = \helper\SqlBuilder\Builder::select($table, $fields);
var_dump($sql);

?>
<h2>Test 2</h2>
Select certain fields from table conditionally
<?php
$table = 'employment_table';
var_dump($table);
$fields = [
    'name' => "CONCAT(firstname, ' ', lastname)",
    'age'
];
var_dump($fields);
$condition = new \helper\SqlBuilder\ConditionAnd(
    new \helper\SqlBuilder\ConditionGreaterOrEqual('age', 18),
    new \helper\SqlBuilder\ConditionLessThan('age', 65),
    new \helper\SqlBuilder\ConditionOr(
        new \helper\SqlBuilder\ConditionNull('last_employer'),
        new \helper\SqlBuilder\ConditionLessThan('last_employment_end', 'DATE(NOW)')
    )
);
var_dump($condition);

$sql = \helper\SqlBuilder\Builder::select($table, $fields, $condition);
var_dump($sql);

?>
<h2>Test 3</h2>
Insert into
<?php
$table = 'financial_table';
var_dump($table);
$fields = [
    'transaction_type' => "'expense'",
    'amount' => 387.5,
    'currency' => 'EUR'
];
var_dump($fields);

$sql = \helper\SqlBuilder\Builder::insert($table, $fields);
var_dump($sql);

?>
<h2>Test 4</h2>
Update
<?php
$table = 'people_table';
var_dump($table);
$fields = [
    'firstname' => "'Jokurt'"
];
var_dump($fields);
$condition = new \helper\SqlBuilder\ConditionEquals('firstname', "'Kurt'");
var_dump($condition);

$sql = \helper\SqlBuilder\Builder::update($table, $fields, $condition);
var_dump($sql);

?>
<h2>Test 5</h2>
Delete From
<?php
$table = 'people_alive';
var_dump($table);
$condition = new \helper\SqlBuilder\ConditionEquals('nationality', "'Palestine'");
var_dump($condition);

$sql = \helper\SqlBuilder\Builder::delete($table, $condition);
var_dump($sql);

