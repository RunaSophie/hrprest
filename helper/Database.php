<?php
namespace helper;

function db_connect() : \PDO {
    $dbparams = parse_ini_file(dirname(__DIR__) . 'hrprest.ini', true, INI_SCANNER_TYPED)['database'];

    $dbsrv = $dbparams['server'];
    $dbport = $dbparams['port'];
    $dbname = $dbparams['database'];
    $dsn = "mysql:host=$dbsrv;port=$dbport;dbname=$dbname;charset=UTF-8";
    $username = $dbparams['username'];
    $password = $dbparams['password'];

    return new \PDO($dsn, $username, $password);
}
