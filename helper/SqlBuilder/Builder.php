<?php
namespace helper\SqlBuilder;

require_once dirname(dirname(__DIR__)) . '/exception/SqlBuilderException.php';

foreach (glob(__DIR__ . '/Condition*.php', GLOB_NOSORT) as $file) {
    require_once $file;
}

final class Builder {

    private function __construct() {}

    public static function select(string $table, array $fields, Condition $where = null) : string {
        foreach ($fields as $fieldname => $field) {
            if (!is_string($field)) {
                throw new \exception\SqlBuilderException('Field "' . $fieldname . '": "' . $field . '" is invalid!');
            }
        }

        $sql = 'SELECT ';
        $sql .= implode(
            ', ',
            array_map(
                function ($key, $value) { return is_numeric($key) ? $value : $value . ' AS ' . $key; },
                array_keys($fields), array_values($fields)
            )
        );

        $sql .= ' FROM ' . $table;
        if (isset($where)) {
            $sql .= ' WHERE ' . (string)$where;
        }
        return $sql;
    }

    public static function insert(string $table, array $dataset) : string {
        $sql = 'INSERT INTO ' . $table;
        $sql .= ' (' . implode(', ', array_keys($dataset)) . ')';
        $sql .= ' VALUES (' . implode(', ', array_values($dataset)) . ')';
        return $sql;
    }

    public static function update(string $table, array $updated_dataset, Condition $where) : string {
        $sql = 'UPDATE ' . $table . ' SET ';

        $sql .= implode(
                    ', ',
                    array_map(
                        function ($key, $value) { return $key . ' = ' . $value; },
                        array_keys($updated_dataset), array_values($updated_dataset)
                    )
                );

        $sql .= ' WHERE ' . (string)$where;
        return $sql;
    }

    public static function delete(string $table, Condition $where) : string {
        $sql = 'DELETE FROM ' . $table;
        $sql .= ' WHERE ' . (string)$where;
        return $sql;
    }
}