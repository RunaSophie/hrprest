<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition.php';

abstract class Condition2Op extends Condition {
    /**
     * @var Condition|string $operand1
     */
    public $operand1;

    /**
     * @var Condition|string $operand2
     */
    public $operand2;

    public function __construct($operand1, $operand2) {
        $this->operand1 = $operand1;
        $this->operand2 = $operand2;
    }

    public function checkOperands() : void {
        if (!(
               is_a($this->operand1, Condition::class)
            || is_string($this->operand1)
            || is_numeric($this->operand1)
        )) {
            throw new \exception\SqlBuilderException("Operand invalid! $this->operand1 is not of type Condition and neither string nor numeric");
        }

        if (!(
               is_a($this->operand2, Condition::class)
            || is_string($this->operand2)
            || is_numeric($this->operand2)
        )) {
            throw new \exception\SqlBuilderException("Operand invalid! $this->operand2 is not of type Condition and neither string nor numeric");
        }
    }
}