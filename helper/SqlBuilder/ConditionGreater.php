<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition2Op.php';

class ConditionGreater extends Condition2Op {
    public function buildString() : string {
        return '(' . (string)$this->operand1 . ' > ' . (string)$this->operand2 . ')';
    }
}