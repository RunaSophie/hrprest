<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/ConditionNOp.php';

class ConditionOr extends ConditionNOp {
    public function buildString() : string {
        return '(' . implode(' OR ', $this->operands) . ')';
    }
}