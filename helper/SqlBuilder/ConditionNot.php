<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition1Op.php';

class ConditionNot extends Condition1Op {
    public function buildString() : string {
        return '!' . (string)$this->operand;
    }
}