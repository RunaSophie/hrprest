<?php
namespace helper\SqlBuilder;

abstract class Condition {
    public abstract function checkOperands() : void;
    public abstract function buildString() : string;

    public function __toString() : string {
        $this->checkOperands();

        return $this->buildString();
    }
}