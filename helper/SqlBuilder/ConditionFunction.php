<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition.php';

class ConditionFunction extends Condition {

    /**
     * @var string $function
     */
    public $function;

    /**
     * @var array $params
     */
    public $params;

    public function __construct(string $function, ...$params)
    {
        $this->function = $function;
        $this->params = $params;
    }

    public function checkOperands() : void {
        if (!is_string($this->function)) {
            throw new \exception\SqlBuilderException("Operand invalid! $this->function is not of type string");
        }

        foreach ($this->params as $param) {
            if (!(
                   is_a($param, Condition::class)
                || is_string($param)
                || is_numeric($param)
            )) {
                throw new \exception\SqlBuilderException("Operand invalid! $param is not of type Condition and neither string nor numeric");
            }
        }
    }

    public function buildString() : string {
        return $this->function . '(' . implode(', ', $this->params) . ')';
    }
}