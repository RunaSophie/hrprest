<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/ConditionNOp.php';

class ConditionAnd extends ConditionNOp {
    public function buildString() : string {
        return '(' . implode(' AND ', $this->operands) . ')';
    }
}