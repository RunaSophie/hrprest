<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition1Op.php';

class ConditionNotNull extends Condition1Op {
    function buildString() : string {
        return $this->operand . ' IS NOT NULL';
    }
}