<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition.php';

abstract class Condition1Op extends Condition {
    /**
     * @var Condition|string $operand
     */
    public $operand;

    public function __construct($operand) {
        $this->operand = $operand;
    }

    public function checkOperands() : void {
        if (!(
               is_a($this->operand, Condition::class)
            || is_string($this->operand)
            || is_numeric($this->operand)
        )) {
            throw new \exception\SqlBuilderException("Operand invalid! $this->operand is not of type Condition and neither string nor numeric");
        }
    }
}