<?php
namespace helper\SqlBuilder;

require_once __DIR__ . '/Condition.php';

abstract class ConditionNOp extends Condition {
    /**
     * @var array $operand
     */
    public $operands;

    public function __construct(...$operands)
    {
        $this->operands = $operands;
    }

    public function checkOperands() : void {
        foreach ($this->operands as $op) {
            if (!(
                   is_a($op, Condition::class)
                || is_string($op)
                || is_numeric($op)
            )) {
                throw new \exception\SqlBuilderException("Operand invalid! $op is not of type Condition and neither string nor numeric");
            }
        }
    }
}