<?php
namespace helper;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($no_json)) {
    header("Content-Type: application/json; charset=UTF-8");
}

require_once __DIR__ . '/Database.php';

/**
 * Initialise the database connection with the saved config.
 * @throws \exception\DatabaseException if the connection or the first request fails
 * @return \PDO the PDO object to be used during the entire transaction
 */
function init_db() : \PDO {
    try {
        $pdo = \helper\db_connect();
        $sql = 'DELETE FROM auth_key WHERE expires <= NOW()';
        $pdo->query($sql);
        return $pdo;
    } catch(\PDOException $ex) {
        throw new \exception\DatabaseException(null, $ex);
    }
}