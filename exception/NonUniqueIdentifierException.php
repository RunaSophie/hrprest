<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class NonUniqueIdentifierException extends HrpException
{
    public function __construct(string $class, string $varname, int $code = 0, \Exception $previous = null)
    {
        parent::__construct(400, "The parameter '$varname' is not unique as demanded by $class.", $code, $previous);
    }
}