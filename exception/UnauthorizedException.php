<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class UnauthorizedException extends HrpException
{

    public function __construct(string $message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct(401, $message, $code, $previous);
    }
}

