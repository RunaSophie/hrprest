<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class NotFoundException extends HrpException
{
    public function __construct(string $class = 'object', int $code = 0, \Exception $previous = null)
    {
        parent::__construct(404, "The requested $class could not be found.", $code, $previous);
    }
}

