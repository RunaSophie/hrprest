<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class InvalidArgumentException extends HrpException
{

    public function __construct(string $argument, string $type, int $code = 0, \Exception $previous = null)
    {
        parent::__construct(400, "$argument must be of type $type.", $code, $previous);
    }
}

