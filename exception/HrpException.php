<?php
namespace exception;

class HrpException extends \Exception
{
    private $httpCode;

    private const HTTP_ERROR_MESSAGES = [
        400 => 'BAD REQUEST',
        401 => 'UNAUTHORIZED',
        403 => 'FORBIDDEN',
        404 => 'NOT FOUND',
        405 => 'METHOD NOT ALLOWED',
        500 => 'INTERNAL SERVER ERROR'
    ];

    /**
     * Throwable class constructor
     * @param int $httpCode HTTP code of the occurred error
     * @param string $message Error message
     * @param mixed $code PHP internal error code
     * @param \Exception $previous Previous exception
     */
    public function __construct(int $httpCode, string $message, int $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->httpCode = $httpCode;
    }

    public function getHttpCode() : int {
        return $this->httpCode;
    }

    public function getJson() : array {
        $body = [
            'code' => $this->httpCode,
            'error' => $this::HTTP_ERROR_MESSAGES[$this->httpCode],
            'message' => $this->message
        ];

        return $body;
    }
}

