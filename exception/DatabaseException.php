<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class DatabaseException extends HrpException
{

    public function __construct(int $code = 0, \Exception $previous = null)
    {
        parent::__construct(500,
            'Database access failed' . is_a($previous, \Exception::class) ? ': ' . $previous->getMessage() : '.',
            $code, $previous);
    }
}

