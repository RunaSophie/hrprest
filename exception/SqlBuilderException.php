<?php
namespace exception;

require_once __DIR__ . '/HrpException.php';

class SqlBuilderException extends HrpException {

    public function __construct(string $message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct(500, $message, $code, $previous);
    }
}