<?php
namespace db_access;

require_once dirname(__DIR__) . '/helper/SqlBuilder/Builder.php';

class User
{
    /**
     * The PDO connection this class works with
     * @var \PDO $pdo
     */
    private $pdo;

    /**
     * The logged in user's ID
     * @var int $user_id
     */
    public $user_id;

    /**
     * Class constructor
     * @param \PDO $pdo The PDO Connection to work with
     */
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    /**
     * Get a certain user
     * @param string|int $term As string: The username or email to get the user of; As int: The ID of the user to get
     * @return \model\User The user data
     * @throws \exception\InvalidArgumentException If $term is of an invalid type
     */
    public function fetch($term) : \model\User {
        $fields = [
            'user_id', 'username', 'email', 'password_hash',
            'public_key', 'private_key', 'default_pwgen_preset'
        ];

        if (is_int($term)) {
            try {
                $condition = new \helper\SqlBuilder\ConditionEquals('user_id', ':id');
                $execparams = [':id' => $term];

                $sql = \helper\SqlBuilder\Builder::select('hrp_user', $fields, $condition);

                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($execparams);
                $stmt->setFetchMode(\PDO::FETCH_CLASS, \model\User::class);
                if ($stmt->rowCount() < 1) {
                    throw new \exception\NotFoundException('User');
                } else if ($stmt->rowCount() > 1) {
                    throw new \exception\NonUniqueIdentifierException('User', 'user_id');
                } else {
                    $result = $stmt->fetch();
                    return $result;
                }
            } catch (\PDOException $ex) {
                throw new \exception\DatabaseException(0, $ex);
            }
        } else if (is_string($term)) {
            try {
                $condition = new \helper\SqlBuilder\ConditionOr(
                    new \helper\SqlBuilder\ConditionEquals('username', ':unem'),
                    new \helper\SqlBuilder\ConditionEquals('email', ':unem')
                );
                $execparams = [':unem' => $term];

                $sql = \helper\SqlBuilder\Builder::select('hrp_user', $fields, $condition);

                $stmt = $this->pdo->prepare($sql);
                $stmt->execute($execparams);
                $stmt->setFetchMode(\PDO::FETCH_CLASS, \model\User::class);
                if ($stmt->rowCount() < 1) {
                    throw new \exception\NotFoundException('User');
                } else if ($stmt->rowCount() > 1) {
                    throw new \exception\NonUniqueIdentifierException('User', 'username_email');
                } else {
                    $result = $stmt->fetch();
                    return $result;
                }
            } catch (\PDOException $ex) {
                throw new \exception\DatabaseException(0, $ex);
            }
        } else {
            throw new \exception\InvalidArgumentException('term', 'string|int');
        }
    }

    /**
     * Search for users by parts of their username or e-mail
     * @param string $username_email The search term
     * @param ?bool $get_deleted TRUE: only deleted will show; FALSE: deleted will not show; NULL: show all, independent of deletion status
     * @return array Array with every \model\User matching the search term
     */
    public function search(string $username_email, ?bool $get_deleted = false) : array {
        $fields = [
            'user_id', 'username', 'email', 'password_hash',
            'public_key', 'private_key', 'default_pwgen_preset'
        ];

        $condition =
            new \helper\SqlBuilder\ConditionOr(
                new \helper\SqlBuilder\ConditionLike(
                    'username',
                    new \helper\SqlBuilder\ConditionFunction('CONCAT', "'%'", ':unem', "'%'")
                ),
                new \helper\SqlBuilder\ConditionLike(
                    'email',
                    new \helper\SqlBuilder\ConditionFunction('CONCAT', "'%'", ':unem', "'%'")
                )
            );

        switch ($get_deleted) {
            case true:
                $condition = new \helper\SqlBuilder\ConditionAnd(
                    $condition,
                    new \helper\SqlBuilder\ConditionEquals('status', "'dropped'")
                );
                break;
            case false:
                $condition = new \helper\SqlBuilder\ConditionAnd(
                    $condition,
                    new \helper\SqlBuilder\ConditionUnequals('status', "'dropped'")
                );
                break;
        }

        $execparams = [
            ':unem' => $username_email
        ];

        try {
            $sql = \helper\SqlBuilder\Builder::select('hrp_user', $fields, $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
            $stmt->setFetchMode(\PDO::FETCH_CLASS, \model\User::class);
            $result = $stmt->fetchAll();
            return $result;
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * List all users
     * @param ?bool $get_deleted TRUE: only deleted will show; FALSE: deleted will not show; NULL: show all, independent of deletion status
     * @return array Array with every registered \model\User
     */
    public function get_all(?bool $get_deleted = false) : array {
        $fields = [
            'user_id', 'username', 'email', 'password_hash',
            'public_key', 'private_key', 'default_pwgen_preset'
        ];

        switch ($get_deleted) {
            case true:
                $condition = new \helper\SqlBuilder\ConditionEquals('status', "'dropped'");
                break;
            case false:
                $condition = new \helper\SqlBuilder\ConditionUnequals('status', "'dropped'");
                break;
        }

        try {
            $sql = \helper\SqlBuilder\Builder::select('hrp_user', $fields, $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, \model\User::class);
            $result = $stmt->fetchAll();
            return $result;
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * Create a new user
     * @param \model\User $user The user to be created
     */
    public function create(\model\User $user) : void {
        $fields = [];
        $execparams = [];

        if (isset($user->username)) {
            $fields['username'] = ':un';
            $execparams[':un'] = $user->username;
        } else {
            throw new \exception\InvalidArgumentException('username', 'string');
        }

        if (isset($user->email)) {
            $fields['email'] = ':em';
            $execparams[':em'] = $user->email;
        }

        if (isset($user->password_hash)) {
            $fields['password_hash'] = ':pwh';
            $execparams[':pwh'] = $user->password_hash;
        } else {
            throw new \exception\InvalidArgumentException('password_hash', 'string');
        }

        if (isset($user->public_key)) {
            $fields['public_key'] = ':pubk';
            $execparams[':pubk'] = $user->public_key;
        } else {
            throw new \exception\InvalidArgumentException('public_key', 'string');
        }

        if (isset($user->private_key)) {
            $fields['private_key'] = ':prik';
            $execparams[':prik'] = $user->private_key;
        } else {
            throw new \exception\InvalidArgumentException('private_key', 'string');
        }

        if (isset($user->default_pwgen_preset)) {
            $fields['default_pwgen_preset'] = ':pwgpr';
            $execparams[':pwgpr'] = $user->default_pwgen_preset;
        }

        if (isset($user->status)) {
            $fields['status'] = ':st';
            $execparams[':st'] = $user->status;
        }

        try {
            $sql = \helper\SqlBuilder\Builder::insert('hrp_user', $fields);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * Update the data of an existing user
     * @param int $id The ID of the user to update
     * @param \model\User $user The data of the user
     */
    public function update(int $id, \model\User $user) : void {
        $fields = [];
        $condition = new \helper\SqlBuilder\ConditionEquals('user_id', ':uid');
        $execparams = [':uid' => $id];

        if (isset($user->username)) {
            $fields['username'] = ':un';
            $execparams[':un'] = $user->username;
        }

        if (isset($user->email)) {
            $fields['email'] = ':em';
            $execparams[':em'] = $user->email;
        }

        if (isset($user->password_hash)) {
            $fields['password_hash'] = ':pwh';
            $execparams[':pwh'] = $user->password_hash;
        }

        if (isset($user->public_key)) {
            $fields['public_key'] = ':pubk';
            $execparams[':pubk'] = $user->public_key;
        }

        if (isset($user->private_key)) {
            $fields['private_key'] = ':prik';
            $execparams[':prik'] = $user->private_key;
        }

        if (isset($user->default_pwgen_preset)) {
            $fields['default_pwgen_preset'] = ':pwgpr';
            $execparams[':pwgpr'] = $user->default_pwgen_preset;
        }

        if (isset($user->status)) {
            $fields['status'] = ':st';
            $execparams[':st'] = $user->status;
        }

        try {
            $sql = \helper\SqlBuilder\Builder::update('hrp_user', $fields, $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * Reversibly delete a user
     * @param int $id The ID of the user to delete
     */
    public function drop(int $id) : void {
        $fields = ['status' => ':st'];
        $condition = new \helper\SqlBuilder\ConditionEquals('user_id', ':uid');
        $execparams = [':uid' => $id, ':st' => 'dropped'];

        try {
            $sql = \helper\SqlBuilder\Builder::update('hrp_user', $fields, $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * Undo deletion of a dropped user
     * @param int $id The ID of the dropped user
     */
    public function undrop(int $id) : void {
        $fields = ['status' => ':st'];
        $condition = new \helper\SqlBuilder\ConditionEquals('user_id', ':uid');
        $execparams = [':uid' => $id, ':st' => 'active'];

        try {
            $sql = \helper\SqlBuilder\Builder::update('hrp_user', $fields, $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }

    /**
     * Irreversibly delete a user
     * @param int $id The ID of the user to delete
     */
    public function delete_forever(int $id) : void {
        $condition = new \helper\SqlBuilder\ConditionEquals('user_id', ':uid');
        $execparams = [':uid' => $id];

        try {
            $sql = \helper\SqlBuilder\Builder::delete('hrp_user', $condition);

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($execparams);
        } catch (\PDOException $ex) {
            throw new \exception\DatabaseException(0, $ex);
        }
    }
}

