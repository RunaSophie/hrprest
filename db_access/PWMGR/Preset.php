<?php
namespace db_access\PWMGR;

require_once dirname(__DIR__) . '/helper/SqlBuilder/Builder.php';

class Preset
{
    /**
     * The PDO connection this class works with
     * @var \PDO $pdo
     */
    private $pdo;
    
    /**
     * The logged in user's ID
     * @var int $user_id
     */
    public $user_id;
    
    /**
     * Class constructor
     * @param \PDO $pdo The PDO Connection to work with
     */
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    
    /**
     * Get a preset by ID
     * @param int $id The preset ID
     * @return \model\PWMGR\Preset The preset data
     */
    public function fetch(int $id) : \model\PWMGR\Preset {
        
    }
    
    /**
     * Search for a preset by name
     * @param string $term Term to search for
     * @return array Array of every preset containing the given term
     */
    public function search(string $term) : array {
        
    }
    
    /**
     * Get all presets
     * @return array Array of every preset
     */
    public function get_all() : array {
        
    }
    
    /**
     * Create a new preset
     * @param \model\PWMGR\Preset $preset The preset data
     * @return \model\PWMGR\Preset The freshly fetched preset data
     */
    public function create(\model\PWMGR\Preset $preset) : void {
        
    }
    
    /**
     * Update an existing preset
     * @param int $id The preset ID
     * @param \model\PWMGR\Preset $preset The preset data
     * @return \model\PWMGR\Preset The freshly fetched preset data
     */
    public function update(int $id, \model\PWMGR\Preset $preset) : void {
        
    }
    
    /**
     * Delete a preset
     * @param int $id The preset ID
     */
    public function delete(int $id) : void {
        
    }
}

