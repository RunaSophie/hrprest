<?php
namespace db_access\PWMGR;

require_once dirname(__DIR__) . '/helper/SqlBuilder/Builder.php';

class Credential
{
    /**
     * The PDO connection this class works with
     * @var \PDO $pdo
     */
    private $pdo;
    
    /**
     * The logged in user's ID
     * @var int $user_id
     */
    public $user_id;
    
    /**
     * Class constructor
     * @param \PDO $pdo The PDO Connection to work with
     */
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    
    /**
     * Get a credential by ID
     * @param int $id The credential ID
     * @return \model\PWMGR\Credential The credential data
     */
    public function fetch(int $id) : \model\PWMGR\Credential {
        
    }
    
    /**
     * Get all credentials of the given user
     * @param ?bool $get_deleted TRUE: only deleted will show; FALSE: deleted will not show; NULL: show all, independent of deletion status
     * @return array Array with every credential the user has access to
     */
    public function get_all(?bool $get_deleted = false) : array {
        
    }
    
    /**
     * Get every user that has access to the given credential
     * @param int $credential_id
     * @return array Array of users with access to the given credential
     */
    public function get_trusted(int $credential_id) : array {
        
    }
    
    /**
     * Create a new credential
     * @param \model\PWMGR\Credential $credential The credential to be created
     */
    public function create(\model\PWMGR\Credential $credential) : void {
        
    }
    
    /**
     * Update an existing credential
     * @param int $id The credential ID
     * @param \model\PWMGR\Credential $credential The credential data
     */
    public function update(int $id, \model\PWMGR\Credential $credential) : void {
        
    }
    
    /**
     * Reversibly delete a credential
     * @param int $id The ID of the credential to delete
     */
    public function drop(int $id) : void {
        
    }
    
    /**
     * Undo deletion of a dropped credential
     * @param int $id The ID of the dropped credential
     */
    public function undrop(int $id) : void {
        
    }
    
    /**
     * Irreversibly delete a credential
     * @param int $id The ID of the credential to delete
     */
    public function delete_forever(int $id) : void {
        
    }
}

