<?php
namespace db_access\PWMGR;

require_once dirname(__DIR__) . '/helper/SqlBuilder/Builder.php';

class Category
{
    /**
     * The PDO connection this class works with
     * @var \PDO $pdo
     */
    private $pdo;
    
    /**
     * The logged in user's ID
     * @var int $user_id
     */
    public $user_id;
    
    /**
     * Class constructor
     * @param \PDO $pdo The PDO Connection to work with
     */
    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }
    
    /**
     * Get a category by ID
     * @param int $id The category ID
     * @return \model\PWMGR\Category The category data
     */
    public function fetch(int $id) : \model\PWMGR\Category {
        
    }
    
    /**
     * Get all categories of the given user
     * @return array Array of every category of the given user
     */
    public function get_all() : array {
        
    }
    
    /**
     * Create a new category
     * @param \model\PWMGR\Category $category The category to create. Note: Only the IDs are considered in the credentials array.
     */
    public function create(\model\PWMGR\Category $category) : void {
        
    }
    
    /**
     * Update an existing category
     * @param int $id The category ID
     * @param \model\PWMGR\Category $category The category to create. Note: Only the IDs are considered in the credentials array.
     */
    public function update(int $id, \model\PWMGR\Category $category) : void {
        
    }
    
    /**
     * Irreversibly delete a category
     * @param int $id The category ID
     */
    public function delete(int $id) : void {
        
    }
}

